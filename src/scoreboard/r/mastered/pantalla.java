/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scoreboard.r.mastered;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class pantalla extends javax.swing.JFrame {

    public pantalla() {
        initComponents();
    }

    public static void actualizar(int redscore, int orangescore, int yellowscore, int greenscore) throws InterruptedException {
        txtRedScore.setText("" + redscore);
        txtOrangeScore.setText("" + orangescore);
        txtYellowScore.setText("" + yellowscore);
        txtGreenScore.setText("" + greenscore);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtGreenScore = new javax.swing.JTextField();
        txtOrangeScore = new javax.swing.JTextField();
        txtRedScore = new javax.swing.JTextField();
        txtYellowScore = new javax.swing.JTextField();
        lblJosuee = new javax.swing.JLabel();
        lblJosuee1 = new javax.swing.JLabel();
        lblJosuee2 = new javax.swing.JLabel();
        lblJosuee3 = new javax.swing.JLabel();
        bg = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setMaximumSize(new java.awt.Dimension(800, 600));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setPreferredSize(new java.awt.Dimension(750, 600));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtGreenScore.setFont(new java.awt.Font("Bebas Neue", 0, 185)); // NOI18N
        txtGreenScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtGreenScore.setText("0");
        txtGreenScore.setAutoscrolls(false);
        txtGreenScore.setBorder(null);
        txtGreenScore.setFocusable(false);
        txtGreenScore.setOpaque(false);
        txtGreenScore.setRequestFocusEnabled(false);
        getContentPane().add(txtGreenScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 75, 250, -1));

        txtOrangeScore.setEditable(false);
        txtOrangeScore.setFont(new java.awt.Font("Bebas Neue", 0, 185)); // NOI18N
        txtOrangeScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtOrangeScore.setText("0");
        txtOrangeScore.setBorder(null);
        txtOrangeScore.setFocusable(false);
        txtOrangeScore.setOpaque(false);
        txtOrangeScore.setRequestFocusEnabled(false);
        getContentPane().add(txtOrangeScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(535, 75, 250, -1));

        txtRedScore.setEditable(false);
        txtRedScore.setFont(new java.awt.Font("Bebas Neue", 0, 185)); // NOI18N
        txtRedScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtRedScore.setText("0");
        txtRedScore.setBorder(null);
        txtRedScore.setFocusable(false);
        txtRedScore.setOpaque(false);
        txtRedScore.setRequestFocusEnabled(false);
        getContentPane().add(txtRedScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(535, 335, 250, -1));

        txtYellowScore.setEditable(false);
        txtYellowScore.setFont(new java.awt.Font("Bebas Neue", 0, 185)); // NOI18N
        txtYellowScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtYellowScore.setText("0");
        txtYellowScore.setBorder(null);
        txtYellowScore.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtYellowScore.setFocusable(false);
        txtYellowScore.setOpaque(false);
        txtYellowScore.setRequestFocusEnabled(false);
        getContentPane().add(txtYellowScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 335, 250, -1));

        lblJosuee.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblJosuee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scoreboard/Imgs/logo_blanco_opt2.png"))); // NOI18N
        lblJosuee.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblJosuee.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblJosuee.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(lblJosuee, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 505, 250, 70));

        lblJosuee1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblJosuee1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scoreboard/Imgs/logo_blanco_opt2.png"))); // NOI18N
        lblJosuee1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblJosuee1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblJosuee1.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(lblJosuee1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 505, 250, 70));

        lblJosuee2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblJosuee2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scoreboard/Imgs/logo_blanco_opt2.png"))); // NOI18N
        lblJosuee2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblJosuee2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblJosuee2.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(lblJosuee2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 250, 70));

        lblJosuee3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblJosuee3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scoreboard/Imgs/logo_blanco_opt2.png"))); // NOI18N
        lblJosuee3.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblJosuee3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblJosuee3.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(lblJosuee3, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 20, 250, 70));

        bg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scoreboard/Imgs/bg.png"))); // NOI18N
        getContentPane().add(bg, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pantalla().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bg;
    private javax.swing.JLabel lblJosuee;
    private javax.swing.JLabel lblJosuee1;
    private javax.swing.JLabel lblJosuee2;
    private javax.swing.JLabel lblJosuee3;
    private static javax.swing.JTextField txtGreenScore;
    private static javax.swing.JTextField txtOrangeScore;
    private static javax.swing.JTextField txtRedScore;
    private static javax.swing.JTextField txtYellowScore;
    // End of variables declaration//GEN-END:variables
}
