/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scoreboard.r.mastered;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class controller extends javax.swing.JFrame {

    public static int Multiplier = 1;
    public pantalla Controlador;
    public server svr;
    public final String up = "+";
    public final String down = "-";

    public controller() {
        initComponents();
        this.setLocationRelativeTo(this);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        //int height = (int) screenSize.getHeight();

        //maraña NVM
        Controlador = new pantalla();
        Controlador.dispose();
        Controlador.setLocation(width, 0);
        Controlador.setUndecorated(true);
        Controlador.setVisible(true);
        //maraña NVM
        new Thread(new Runnable() {
            @Override
            public void run() {
                server svr = new server();
                svr.run();
            }
        }).start();
        //maraña NVM Like a pro
        //System.out.println("hola");
    }
    
    public void icon() throws IOException {
        Image i = ImageIO.read(getClass().getResource("scoreboard.Imgs/thumbnailJosuee.png"));
        setIconImage(i);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this
     * code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnStart = new javax.swing.JButton();
        txtGreenScore = new javax.swing.JTextField();
        txtOrangeScore = new javax.swing.JTextField();
        txtRedScore = new javax.swing.JTextField();
        txtYellowScore = new javax.swing.JTextField();
        ipAddress = new javax.swing.JLabel();
        btnUpRed = new javax.swing.JButton();
        btnDownRed = new javax.swing.JButton();
        btnDownOrange = new javax.swing.JButton();
        btnUpOrange = new javax.swing.JButton();
        btnUpYellow = new javax.swing.JButton();
        btnDownYellow = new javax.swing.JButton();
        btnUpGreen = new javax.swing.JButton();
        btnDownGreen = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnStart.setFont(new java.awt.Font("Bebas Neue", 0, 18)); // NOI18N
        btnStart.setText("UPDATE");
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        txtGreenScore.setEditable(false);
        txtGreenScore.setFont(new java.awt.Font("Bebas Neue", 0, 56)); // NOI18N
        txtGreenScore.setForeground(new java.awt.Color(153, 204, 0));
        txtGreenScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtGreenScore.setText("0");
        txtGreenScore.setBorder(null);
        txtGreenScore.setOpaque(false);
        txtGreenScore.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtGreenScoreKeyTyped(evt);
            }
        });

        txtOrangeScore.setEditable(false);
        txtOrangeScore.setFont(new java.awt.Font("Bebas Neue", 0, 56)); // NOI18N
        txtOrangeScore.setForeground(new java.awt.Color(255, 136, 0));
        txtOrangeScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtOrangeScore.setText("0");
        txtOrangeScore.setBorder(null);
        txtOrangeScore.setOpaque(false);
        txtOrangeScore.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOrangeScoreKeyTyped(evt);
            }
        });

        txtRedScore.setEditable(false);
        txtRedScore.setFont(new java.awt.Font("Bebas Neue", 0, 56)); // NOI18N
        txtRedScore.setForeground(new java.awt.Color(245, 28, 34));
        txtRedScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtRedScore.setText("0");
        txtRedScore.setBorder(null);
        txtRedScore.setOpaque(false);
        txtRedScore.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRedScoreKeyTyped(evt);
            }
        });

        txtYellowScore.setEditable(false);
        txtYellowScore.setFont(new java.awt.Font("Bebas Neue", 0, 56)); // NOI18N
        txtYellowScore.setForeground(new java.awt.Color(255, 187, 51));
        txtYellowScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtYellowScore.setText("0");
        txtYellowScore.setBorder(null);
        txtYellowScore.setOpaque(false);
        txtYellowScore.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtYellowScoreKeyTyped(evt);
            }
        });

        ipAddress.setFont(new java.awt.Font("Bebas Neue", 0, 18)); // NOI18N
        ipAddress.setText("192.168.1.1");

        btnUpRed.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnUpRed.setText("+");
        btnUpRed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpRedActionPerformed(evt);
            }
        });

        btnDownRed.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnDownRed.setText("-");
        btnDownRed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownRedActionPerformed(evt);
            }
        });

        btnDownOrange.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnDownOrange.setText("-");
        btnDownOrange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownOrangeActionPerformed(evt);
            }
        });

        btnUpOrange.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnUpOrange.setText("+");
        btnUpOrange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpOrangeActionPerformed(evt);
            }
        });

        btnUpYellow.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnUpYellow.setText("+");
        btnUpYellow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpYellowActionPerformed(evt);
            }
        });

        btnDownYellow.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnDownYellow.setText("-");
        btnDownYellow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownYellowActionPerformed(evt);
            }
        });

        btnUpGreen.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnUpGreen.setText("+");
        btnUpGreen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpGreenActionPerformed(evt);
            }
        });

        btnDownGreen.setFont(new java.awt.Font("Bebas Neue", 0, 24)); // NOI18N
        btnDownGreen.setText("-");
        btnDownGreen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownGreenActionPerformed(evt);
            }
        });

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addComponent(btnStart)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnDownRed, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnUpRed, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtRedScore, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtOrangeScore, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                            .addComponent(btnDownOrange, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnUpOrange, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtYellowScore, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                            .addComponent(btnUpYellow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnDownYellow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(46, 46, 46)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtGreenScore, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                            .addComponent(btnUpGreen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnDownGreen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ipAddress)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtGreenScore, txtOrangeScore, txtRedScore, txtYellowScore});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(ipAddress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpRed, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpOrange, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpYellow, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpGreen, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtGreenScore)
                    .addComponent(txtRedScore)
                    .addComponent(txtOrangeScore)
                    .addComponent(txtYellowScore))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDownRed, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDownOrange)
                    .addComponent(btnDownYellow)
                    .addComponent(btnDownGreen))
                .addGap(33, 33, 33)
                .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnDownGreen, btnDownOrange, btnDownRed, btnDownYellow});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
        int scoreRed = Integer.parseInt(txtRedScore.getText());
        int scoreOrange = Integer.parseInt(txtOrangeScore.getText());
        int scoreYellow = Integer.parseInt(txtYellowScore.getText());
        int scoreGreen = Integer.parseInt(txtGreenScore.getText());

        try {
            Controlador.actualizar(scoreRed, scoreOrange, scoreYellow, scoreGreen);
        } catch (InterruptedException ex) {
            Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnStartActionPerformed

    private void txtGreenScoreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGreenScoreKeyTyped
        char input = evt.getKeyChar();

        if (txtGreenScore.getText().length() > 1 || !Character.isDigit(input)) {
            evt.consume();
        }
    }//GEN-LAST:event_txtGreenScoreKeyTyped

    private void txtOrangeScoreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOrangeScoreKeyTyped
        char input = evt.getKeyChar();

        if (txtOrangeScore.getText().length() > 1 || !Character.isDigit(input)) {
            evt.consume();
        }
    }//GEN-LAST:event_txtOrangeScoreKeyTyped

    private void txtRedScoreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRedScoreKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRedScoreKeyTyped

    private void txtYellowScoreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtYellowScoreKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtYellowScoreKeyTyped

    private void btnUpOrangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpOrangeActionPerformed
        setScore(txtOrangeScore, up);
    }//GEN-LAST:event_btnUpOrangeActionPerformed

    private void btnUpRedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpRedActionPerformed
        setScore(txtRedScore, up);
    }//GEN-LAST:event_btnUpRedActionPerformed

    private void btnDownRedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownRedActionPerformed
       setScore(txtRedScore, down);
    }//GEN-LAST:event_btnDownRedActionPerformed

    private void btnDownOrangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownOrangeActionPerformed
        setScore(txtOrangeScore, down);
    }//GEN-LAST:event_btnDownOrangeActionPerformed

    private void btnUpYellowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpYellowActionPerformed
        setScore(txtYellowScore, up);
    }//GEN-LAST:event_btnUpYellowActionPerformed

    private void btnDownYellowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownYellowActionPerformed
        setScore(txtYellowScore, down);
    }//GEN-LAST:event_btnDownYellowActionPerformed

    private void btnUpGreenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpGreenActionPerformed
        setScore(txtGreenScore, up);
    }//GEN-LAST:event_btnUpGreenActionPerformed

    private void btnDownGreenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownGreenActionPerformed
        setScore(txtGreenScore, down);
    }//GEN-LAST:event_btnDownGreenActionPerformed

    public void setScore(JTextField tf, String d) {
        switch (d) {
            case up:
                Integer score = Integer.parseInt(tf.getText());
                score += Multiplier;
                tf.setText("" + score);
                break;
                
            case down:
                Integer scor = Integer.parseInt(tf.getText());
                scor -= Multiplier;
                tf.setText("" + scor);
                break;
        }
        btnStart.doClick();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new controller().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnDownGreen;
    public static javax.swing.JButton btnDownOrange;
    public static javax.swing.JButton btnDownRed;
    public static javax.swing.JButton btnDownYellow;
    public static javax.swing.JButton btnStart;
    public static javax.swing.JButton btnUpGreen;
    public static javax.swing.JButton btnUpOrange;
    public static javax.swing.JButton btnUpRed;
    public static javax.swing.JButton btnUpYellow;
    public static javax.swing.JLabel ipAddress;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    public static javax.swing.JTextField txtGreenScore;
    public static javax.swing.JTextField txtOrangeScore;
    public static javax.swing.JTextField txtRedScore;
    public static javax.swing.JTextField txtYellowScore;
    // End of variables declaration//GEN-END:variables
}
