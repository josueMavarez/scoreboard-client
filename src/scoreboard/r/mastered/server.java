package scoreboard.r.mastered;

import java.io.BufferedOutputStream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class server {

    private static ServerSocket serverSocket;
    private static Socket clientSocket;
    private static InputStreamReader inputStreamReader;
    private static BufferedReader bufferedReader;
    private static BufferedOutputStream bufferedOutputStream;
    private static OutputStreamWriter outputStreamWriter;
    private static String message;
    public String ipadd;
    public String ip;
    public controller control;
    public pantalla Controlador;

    public void run() {
        try {
            serverSocket = new ServerSocket(4444);
        } catch (IOException e) {
            System.out.println("Nope Port!");
        }

        while (true) {
            try {

                clientSocket = serverSocket.accept();

                ipadd = String.valueOf(clientSocket.getInetAddress());
                ipadd = ipadd.substring(1, (ipadd.length()));
                controller.ipAddress.setText("IP: " + ipadd);
                
                inputStreamReader = new InputStreamReader(clientSocket.getInputStream());
                bufferedReader = new BufferedReader(inputStreamReader);
                message = bufferedReader.readLine();
                
                switch (message) {
                    case "redUp":
                        controller.btnUpRed.doClick();
                        break;
                    case "redDown":
                        controller.btnDownRed.doClick();
                        break;
                    case "orangeUp":
                        controller.btnUpOrange.doClick();
                        break;
                    case "orangeDown":
                        controller.btnDownOrange.doClick();
                        break;
                    case "yellowUp":
                        controller.btnUpYellow.doClick();
                        break;
                    case "yellowDown":
                        controller.btnDownYellow.doClick();
                        break;
                    case "greenUp":
                        controller.btnUpGreen.doClick();
                        break;
                    case "greenDown":
                        controller.btnDownGreen.doClick();
                        break;
                    case "dingBtn":
                        //DING DING DING SHOW IMG + SOUND
                        break;
//                   case "update":
//                        controller.btnStart.doClick();
//                        break;
//                    case "x1":
//                        controller.tglByOne.doClick();
//                        break;
//                    case "x5":
//                        controller.tglByFive.doClick();
//                        break;
//                    case "x10":
//                        controller.tglByTen.doClick();
//                        break;
//                    case "x20":
//                        controller.tglByTwenty.doClick();
//                        break;
//                    case "x50":
//                        controller.tglByFifty.doClick();
//                        break;
                }
                
                OutputStream os = clientSocket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);
                String send = controller.txtGreenScore.getText() + "-"
                          + controller.txtYellowScore.getText() + "-"
                          + controller.txtOrangeScore.getText() + "-"
                          + controller.txtRedScore.getText();
                
                System.out.println(send);
                bw.write(send);
                bw.flush();
                
                controller.btnStart.doClick();
                
                inputStreamReader.close();
                clientSocket.close();

            } catch (IOException ex) {
                System.out.println("Nope!!" + ex);
            }
        }
    }
}
